import express from 'express'
import multer from 'multer'
import { join } from 'path'
import { knex } from './db'

let upload = multer({
  storage: multer.diskStorage({
    destination: join('uploads', 'icons'),
  }),
})

let router = express.Router()

export default router

router.post('/user', upload.single('icon_file'), async (req, res) => {
  console.log('body:', req.body)
  console.log('file:', req.file)
  if (!req.body.name) {
    res.status(400).end('missing name in body')
    return
  }
  let user = req.body
  if (req.file && req.file.mimetype.startsWith('image/')) {
    user.icon = req.file.filename
  }
  try {
    console.log('insert user:', user)
    let rows = await knex.insert(user).into('user').returning('id')
    console.log('rows:', rows)
    res.status(201).json(rows[0])
  } catch (error) {
    res.status(500).json(error.toString())
  }
})

router.patch('/user/:id', upload.single('icon_file'), async (req, res) => {
  if (!req.params.id) {
    res.status(400).end('missing id in params')
    return
  }
  if (!req.body.name) {
    res.status(400).end('missing name in body')
    return
  }
  let user = req.body
  if (req.file && req.file.mimetype.startsWith('image/')) {
    user.icon = req.file.filename
  }
  try {
    console.log('update user:', user)
    await knex('user').update(user).where({ id: req.params.id })
    res.end()
  } catch (error) {
    res.status(500).json(error.toString())
  }
})

router.get('/user/:id', async (req, res) => {
  if (!req.params.id) {
    res.status(400).end('missing id in params')
    return
  }
  try {
    console.log('select user:', req.params.id)
    let user = await knex
      .select('*')
      .from('user')
      .where({ id: req.params.id })
      .first()
    res.json(user)
  } catch (error) {
    res.status(500).json(error.toString())
  }
})
