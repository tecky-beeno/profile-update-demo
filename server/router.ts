import express from 'express'

export let router = express.Router()

router.get('/session', (req, res) => {
  res.json(req.session)
})

import userRouter from './user'
router.use(userRouter)

import postRouter from './post'
router.use(postRouter)

import commentRouter from './comment'
router.use(commentRouter)
