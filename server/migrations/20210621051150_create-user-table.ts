import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('user', t => {
    t.increments()
    t.string('name')
    t.string('icon')
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('user')
}
