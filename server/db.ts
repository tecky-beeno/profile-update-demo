import { config } from 'dotenv'
import Knex from 'knex'
config()

let mode = process.env.NODE_ENV || 'development'

let configs = require('./knexfile')

export let knex = Knex(configs[mode])
