import { useHistory } from 'react-router-dom'

export function Login() {
  const history = useHistory()
  return (
    <div className="page">
      <h1>Login</h1>
      <form
        onSubmit={e => {
          e.preventDefault()
          history.push('/profile')
        }}
      >
        <label htmlFor="user_id">User ID</label>
        <input
          id="user_id"
          type="number"
          onChange={e => {
            localStorage.setItem('user_id', e.target.value)
          }}
        />
      </form>
    </div>
  )
}
export default Login
