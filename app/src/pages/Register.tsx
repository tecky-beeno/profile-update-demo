import { useState } from 'react'
import { useForm } from 'react-hook-form'
import Message from '../components/Message'
import { api } from '../helpers/api'

type Data = {
  icon_file_list: FileList
  name: string
}

export function Register() {
  const form = useForm<Data>()
  const [msg, setMsg] = useState({
    color: 'black',
    text: '',
  })
  async function submit(data: Data) {
    console.log(data)
    if (!data.name) {
      setMsg({
        color: 'red',
        text: 'missing name',
      })
      return
    }
    let formData = new FormData()
    formData.set('name', data.name)
    let file = data.icon_file_list.item(0)
    if (file && file.type.startsWith('image/')) {
      formData.set('icon_file', file)
    }
    setMsg({ color: 'black', text: 'loading...' })
    try {
      let res = await api['/user'].post(formData)
      if (res.status === 201) {
        let id = await res.json()
        setMsg({ color: 'green', text: `Done, your id is ${id}` })
        localStorage.setItem('user_id', id)
      } else {
        setMsg({ color: 'red', text: await res.text() })
      }
    } catch (error) {
      setMsg({ color: 'red', text: error.toString() })
    }
  }
  return (
    <div className="page">
      <h1>Register</h1>
      <form onSubmit={form.handleSubmit(submit)}>
        <label htmlFor="name">Name</label>
        <input id="name" type="text" {...form.register('name')} />
        <label htmlFor="icon_file_list">Icon</label>
        <input
          type="file"
          accept="image/*"
          id="icon_file_list"
          multiple={false}
          {...form.register('icon_file_list')}
        />
        <br />
        <input type="reset" value="Reset" />
        <input type="submit" value="Register" />
      </form>
      <Message {...msg} />
    </div>
  )
}
export default Register
