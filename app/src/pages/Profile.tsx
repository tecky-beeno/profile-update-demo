import { Redirect, useHistory } from 'react-router-dom'
import { api } from '../helpers/api'

export function Profile() {
  let user_id = localStorage.getItem('user_id')
  const history = useHistory()

  if (user_id) {
    api['/user/:id'].get(user_id).then(async res => {
      console.log('res:', res)
      let profile = await res.json()
      console.log('profile:', profile)
    })
  }

  if (!user_id) {
    return <Redirect to="/login" />
  }
  return (
    <div className="page">
      <h1>Profile</h1>
      <code>ID: {user_id}</code>
      <div>
        <button
          onClick={() => {
            localStorage.removeItem('user_id')
            history.replace('/login')
          }}
        >
          Logout
        </button>
      </div>
    </div>
  )
}
export default Profile
