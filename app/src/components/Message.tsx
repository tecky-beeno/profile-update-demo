export function Message(props: { color: string; text: string }) {
  return <p style={{ color: props.color }}>{props.text}</p>
}
export default Message
