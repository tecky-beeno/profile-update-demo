import React from 'react'
import logo from './logo.svg'
import './App.css'
import { Redirect, Switch, Route, Router, Link } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import Login from './pages/Login'
import Register from './pages/Register'
import Profile from './pages/Profile'

let history = createBrowserHistory()

function App() {
  return (
    <div>
      <Router history={history}>
        <nav>
          <ul>
            <li>
              <Link to="/login">Login</Link>
            </li>
            <li>
              <Link to="/register">Register</Link>
            </li>
            <li>
              <Link to="/profile">Profile</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/" exact>
            <Redirect to="/login" />
          </Route>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/profile" component={Profile} />
        </Switch>
      </Router>
    </div>
  )
}

export default App
