const { REACT_APP_SERVER_ORIGIN } = process.env

export function request(method: string) {
  return function (url: string, body?: any) {
    if (url.startsWith('/')) {
      url = REACT_APP_SERVER_ORIGIN + url
    }
    if (body instanceof FormData) {
      return fetch(url, {
        method,
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
        body,
      })
    }
    if (body) {
      return fetch(url, {
        method,
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body || {}),
      })
    }
    return fetch(url, {
      method,
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  }
}

export let get = request('GET')
export let post = request('POST')
export let patch = request('PATCH')
export let del = request('DELETE')
export let put = request('PUT')

export let api = {
  '/user': {
    post: (formData: FormData) => post('/user', formData),
  },
  '/user/:id': {
    patch: (id: string, formData: FormData) => patch(`/user/${id}`, formData),
    get: (id: string) => get(`/user/${id}`),
  },
}
